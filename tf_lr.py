# -*- coding: utf-8 -*-
import mlflow
import mlflow.tensorflow

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

import tensorflow as tf
import tensorflow.keras as keras
import tempfile


import os
import sys


DATA_URI = sys.argv[1]
TRAIN_FRAC = float(sys.argv[2])
L2_WEIGHT = float(sys.argv[3])
NUM_EPOCHS = int(sys.argv[4])
BATCH_SIZE = int(sys.argv[5])
LEARNING_RATE = float(sys.argv[6])


dd = pd.read_csv(DATA_URI,na_values='?')


train = dd.sample(frac=TRAIN_FRAC).copy()
y_train = train['target']
train.drop('target', axis=1, inplace=True)

test = dd.loc[~dd.index.isin(train.index)].copy()
y_test = test['target']
test.drop('target', axis=1, inplace=True)

def create_logreg(feature_columns, feature_layer_inputs, optimizer,
loss='binary_crossentropy', metrics=['accuracy'],
l2=0.01):
    regularizer = keras.regularizers.l2(l2)
    feature_layer = keras.layers.DenseFeatures(feature_columns)
    feature_layer_outputs = feature_layer(feature_layer_inputs)
    norm = keras.layers.BatchNormalization()(feature_layer_outputs)
    outputs = keras.layers.Dense(1,
    kernel_initializer='normal',
    kernel_regularizer = regularizer,
    activation='sigmoid')(norm)
    model = keras.Model(inputs=[v for v in feature_layer_inputs.values()],
    outputs=outputs)
    model.compile(optimizer=optimizer, loss=loss, metrics=metrics)
    return model

def canned_keras(model):
    model_dir = 'logs'
    keras_estimator = tf.keras.estimator.model_to_estimator(
    keras_model=model, model_dir=model_dir)
    return keras_estimator

def make_input_fn(data_df, label_df, num_epochs=10,
    shuffle=True, batch_size=256):
    def input_function():
        ds = tf.data.Dataset.from_tensor_slices((dict(data_df), label_df))
        if shuffle:
            ds = ds.shuffle(1000)
        ds = ds.batch(batch_size).repeat(num_epochs)
        return ds
    return input_function

def define_feature_columns(data_df, categorical_cols, numeric_cols):
    feature_columns = []
    for feature_name in numeric_cols:
        feature_columns.append(tf.feature_column.numeric_column(
        feature_name, dtype=tf.float32))
    for feature_name in categorical_cols:
        vocabulary = data_df[feature_name].unique()

    feature_columns.append(
    tf.feature_column.categorical_column_with_vocabulary_list(
    feature_name,vocabulary))
    return feature_columns

def define_feature_columns_layers(data_df, categorical_cols, numeric_cols):
    feature_columns = []
    feature_layer_inputs = {}
    for feature_name in numeric_cols:
        feature_columns.append(tf.feature_column.numeric_column(feature_name, dtype=tf.float32))
        feature_layer_inputs[feature_name] = tf.keras.Input(shape=(1,),name=feature_name)
    for feature_name in categorical_cols:
        vocabulary = data_df[feature_name].unique()
        cat = tf.feature_column.categorical_column_with_vocabulary_list(feature_name, vocabulary)
        cat_one_hot = tf.feature_column.indicator_column(cat)
        feature_columns.append(cat_one_hot)
        feature_layer_inputs[feature_name] = tf.keras.Input(shape=(1,),
        name=feature_name, dtype=tf.int32)
    return feature_columns, feature_layer_inputs


if __name__ == "__main__":
    mlflow.tensorflow.autolog()
    categorical_cols = ['sex','cp','fbs','restecg','exang','slope','ca','thal']
    numeric_cols = list(set(list(test.columns.values)) - set(categorical_cols))
    numeric_cols

    feature_columns, feature_layer_inputs = define_feature_columns_layers(dd,categorical_cols, numeric_cols)
    optimizer = keras.optimizers.Ftrl(learning_rate=LEARNING_RATE)
    model = create_logreg(feature_columns, feature_layer_inputs, optimizer,l2=L2_WEIGHT)

    estimator = canned_keras(model)
    train_input_fn = make_input_fn(train, y_train, num_epochs=NUM_EPOCHS, batch_size=BATCH_SIZE)
    test_input_fn = make_input_fn(test, y_test, num_epochs=1, shuffle=False)
    with mlflow.start_run() as run:
        estimator.train(train_input_fn)

    result = estimator.evaluate(test_input_fn)
    print(result)

