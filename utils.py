import pandas as pd

def fetch_data():
    return(pd.read_csv('https://raw.githubusercontent.com/rtflynn/Heart-Disease-Model/master/heart.csv',na_values='?'))