import sys
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

import tensorflow as tf
import tensorflow.keras as keras
import datetime
import shutil
import tempfile
import mlflow.tensorflow
import mlflow

dd = pd.read_csv('https://raw.githubusercontent.com/rtflynn/Heart-Disease-Model/master/heart.csv',na_values='?')

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(dd.drop('target', axis=1, inplace=False), dd['target'], test_size=0.2, random_state=42)


mlflow.tensorflow.autolog()
def main(argv):
    with mlflow.start_run():
        model = keras.models.Sequential()
        model.add(keras.layers.Dense(26,activation = 'relu',input_dim = 13))
        model.add(keras.layers.Dense(1,activation = 'sigmoid',input_dim = 26,kernel_regularizer = keras.regularizers.l2(0.01)))

        model.compile(optimizer=keras.optimizers.Ftrl(learning_rate=0.002),
                    loss='binary_crossentropy',
                    metrics=['accuracy'])


        #shutil.rmtree('logs')
        #log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        #tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

        model.fit(x=X_train, 
                y=y_train, 
                epochs=3000, 
                batch_size=64,
                validation_data=(X_test, y_test)) 
                #callbacks=[tensorboard_callback])

        _, accuracy = model.evaluate(X_test, y_test)
        print('Accuracy: %.2f' % (accuracy*100))

if __name__ == "__main__":
    main(sys.argv)

