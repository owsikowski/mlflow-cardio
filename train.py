import os
import warnings
import sys

import numpy as np
import pandas as pd
import sklearn
from sklearn.svm import SVC

from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import AdaBoostClassifier
from sklearn.model_selection import cross_validate

import mlflow
import mlflow.sklearn




def main():
    warnings.filterwarnings("ignore")
    np.random.seed(40)
    cv_num = int(sys.argv[1]) if len(sys.argv)>1 else 5

    mlflow.sklearn.autolog()

    dd = pd.read_csv('https://raw.githubusercontent.com/rtflynn/Heart-Disease-Model/master/heart.csv',na_values='?')
    y=dd.iloc[:,-1]
    X = dd.iloc[:, :-1]
    gs = GridSearchCV(AdaBoostClassifier(),n_jobs=-1, param_grid={"n_estimators": [1,2,5,10,15,20,30,40,50,60,70,80,90,100,120,150,200]})

    

    with mlflow.start_run() as run:
        gs.fit(X,y)
        cv_results = cross_validate(gs, X, y, cv=cv_num, return_train_score=True, scoring=['accuracy','balanced_accuracy','precision','f1','recall'])
        print("Logged data and model in run {}".format(run.info.run_id))

if __name__ == "__main__":
    main()
