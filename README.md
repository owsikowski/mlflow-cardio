# MLflow project for Heart Disorder Prognosis 
## A small programming exercise to get a grasp of numpy, tensorflow and mlflow.
The idea was from "Heart Disorder Prognosis Employing
KNN, ANN, ID3 and SVM" from
"Jyoti Deshmukh, Mukul Jangid, Shreeshail Gupte, and Siddhartha Ghosh", page 513 from
Advanced
Machine Learning
Technologies and
Applications
Proceedings of AMLTA 2020
Advances in Intelligent Systems and Computing
Volume 1141".
Not everything was clear there for me 
so I thought that it would be a good idea to practice my ML skills and see wether I can get similar results.
The work consists of 3 notebooks and this github-repository which serves as a mlflow project.
## Procedure
### Taking a first look at the data with pandas and sklearn
First I wanted to have a look at the data and try some classic models with scikit-learn.
The notebook for this can be found at:\
 https://www.kaggle.com/marekowsikowski/cardiosklearn
### Logistic Regression Estimator with TensorFlow
My next step was to create a model in TensorFlow in order to get similar or better predictions then with the classic ones. As a warmup I wanted to implement first logistic regression before continuing with DNN.
This was a little more complicated then I thought since I used tf.keras.estimator.
The benefit was that I learned more about TensorFlow's working.
The notebook is here:\
 https://colab.research.google.com/drive/1sZi6SLmx5u76Alzjep7CcPg01pSpK0pR?usp=sharing
 ### DNN with TensorFlow
The next step was to create a DNN with one hidden layer. In order to learn something I tried to simplify and shorten the code. You can see it here:\ 
https://colab.research.google.com/drive/1CMwHQCxHe1cGKX6Fa-e6Lgx0hUxQFxp1?usp=sharing
## MLOps
Above notebooks were not meant to be exhaustive, they served educational purposes for myself.
If one wishes to further optimize these models, an organized way should be preferred to plain notebooks.
That's why I created this git repository which serves as an mlflow project.
In order to install mlflow you do:
```
pip3 install mlflow
```
The repository contains the resulting models from the notebooks which can be run with:
```
#for the AdaBoostClassifier
mlflow run https://gitlab.com/owsikowski/mlflow-cardio 
#or if you want a parameter
mlflow run https://gitlab.com/owsikowski/mlflow-cardio -P alpha=5.0
mlflow run https://gitlab.com/owsikowski/mlflow-cardio -P cv=5
#LogisticRegression with TensorFlow with default parameters
mlflow run https://gitlab.com/owsikowski/mlflow-cardio -e tflr
#LogisticRegression with TensorFlow with non-default parameters
mlflow run https://gitlab.com/owsikowski/mlflow-cardio -e tflr -P data_uri=https://raw.githubusercontent.com/rtflynn/Heart-Disease-Model/master/heart.csv -P train_frac=0.7 -P l2_weight=0.005 -P num_epochs=200 -P batch_size=20 -P learning_rate=0.01
#DNN with TensorFlow with default parameters
mlflow run https://gitlab.com/owsikowski/mlflow-cardio -e tfdnn
```

With this tool you can monitor progress of all test runs and performances etc. You can also write a script(with sh or python) to run tests for hypertuning parameters.
In an productive environment you can start it with cron or Apache Airflow.
It is however a good starting point since you can track work progress. Old models can be tweaked or new models added and it works well with git.
If you want to track it by yourself you can use:
```
mlflow ui
```
and view it at http://localhost:5000 \
For more complete usage please check the mlflow documentation. \
## Further possible development
If there's enough data one could think of making a seperate data-fetch task. \
It's also possibble to configure an mlflow project to have it dockerified and run in a kubernetes cluster in case more processing power is required. The container will log everything to the MLflow tracking server.

